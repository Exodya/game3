<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('/api/games' , 'GameController@index');

Route::post('/api/games' , 'GameController@store');

Route::get('/api/games/{game}' , 'GameController@show');

Route::patch('/api/games/{game}' , 'GameController@update');

Route::delete('/api/games/{game}' , 'GameController@destroy');