<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class GameResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'version' => $this->version,
            'server_size' => $this->server_size,
            'server_location' => $this->server_location,
            'game_type' => $this->game_type,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            ];
    }
}
